export async function onRequest(context, next) {
    try {
        return await next();
    } catch (error) {
        if (error instanceof Response) {
            return error;
        }
        
        throw error;
    }
};
