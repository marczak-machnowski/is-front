export function getStaticPaths() {
    return [
        { params: { category: 'inflation' } },
        { params: { category: 'maturityExamPassRate' } },
        { params: { category: 'maturityExamAverageGrades' } },
        { params: { category: 'economicActivity' } },
    ];
};

export const get = async ({ cookies, params, }) => {
    const { category } = params;

    const response = await fetch(`${import.meta.env.BACKEND_URL}/${category}/all`, {
        headers: {
            'content-type': 'application/xml',
            authorization: `Bearer ${cookies.get('accessToken').value ?? ''}`,
        },
    });

    return new Response(await response.text(), {
        status: response.status,
    });
};

export async function del({ params, cookies, }) {
    const { category } = params;

    const response = await fetch(`${import.meta.env.BACKEND_URL}/${category}/all`, {
        method: 'DELETE',
        headers: {
            authorization: `Bearer ${cookies.get('accessToken').value ?? ''}`,
        },
    });

    return new Response(undefined, {
        status: response.status,
    });
};
