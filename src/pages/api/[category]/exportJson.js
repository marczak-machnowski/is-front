export function getStaticPaths() {
    return [
        { params: { category: 'inflation' } },
        { params: { category: 'maturityExamPassRate' } },
        { params: { category: 'maturityExamAverageGrades' } },
        { params: { category: 'economicActivity' } },
    ];
};

export const get = async ({ cookies, params, }) => {
    const { category } = params;

    const response = await fetch(`${import.meta.env.BACKEND_URL}/${category}/all`, {
        headers: {
            accept: 'application/json',
            authorization: `Bearer ${cookies.get('accessToken').value ?? ''}`,
        },
    });

    if (response.ok) {
        const timestamp = new Date().toISOString().replaceAll('-', '_').replaceAll(':', '_').replaceAll('.', '_');

        return new Response(response.body, {
            headers: {
                'content-disposition': `attachement; filename="${category}_${timestamp}.json"`,
            },
        });
    }

    return new Response(undefined, {
        status: response.status,
    });
};
