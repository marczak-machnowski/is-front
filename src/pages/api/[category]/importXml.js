export function getStaticPaths() {
    return [
        { params: { category: 'inflation' } },
        { params: { category: 'maturityExamPassRate' } },
        { params: { category: 'maturityExamAverageGrades' } },
        { params: { category: 'economicActivity' } },
    ];
};

export const post = async ({ cookies, params, request, }) => {
    const { category } = params;

    const response = await fetch(`${import.meta.env.BACKEND_URL}/${category}/all`, {
        method: 'POST',
        headers: {
            'content-type': 'application/xml',
            authorization: `Bearer ${cookies.get('accessToken').value ?? ''}`,
        },
        body: request.body,
        duplex: 'half',
    });

    return new Response(undefined, {
        status: response.status,
    });
};
